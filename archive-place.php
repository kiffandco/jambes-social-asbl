<?php
  $queryAgrs = array(
    'post_type'=>'place',
    'posts_per_page'=>-1,
    'suppress_filters' => false
  );

  $placeGridItems = get_posts ($queryAgrs);
  if (!empty($placeGridItems)):
?>

  <section id="place" class="container">

    <header>
      <h1><?php _e('Location de salles <br>sur Namur','moulinseventeen'); ?></h1>
    </header>

    <?php
      set_query_var( 'placeGridItems', $placeGridItems);
      get_template_part('templates/place-grid');
    ?>

  </section>

<?php
  endif;
?>

<script>
  jQuery(document).ready(
    function(){
      jQuery(".fittable").fit()

    }

  )
</script>
