<!-- SECTION SERVICES -->
<header class="text-center padding-top-medium padding-bottom-medium">

  <h1>Au quotidien</h1>

</header>

<section class="hilight-services clearfix">

  <?php
    $args = array(
      'type'                     => 'services',
      'orderby'                  => 'ID',
      'order'                    => 'ASC',
      'hide_empty'               => 1,
      'hierarchical'             => 1,
      'taxonomy'                 => 'servicestype',
      'pad_counts'               => false
    );

    $categories = get_categories( $args );

    foreach ( $categories as $cat ) :

      $color            = get_field('tax_services_color', $cat);
      $image            = get_field('tax_services_image_bg', $cat);

  ?>

    <article class="item col-sm-6 col-xs-12 animated fade-to-top">

      <div class="bg-color" style="background: <?php echo $color; ?>;"></div>
      <div class="image-wrapper" style="background: url('<?php echo $image['url'];?>') no-repeat center/cover;"></div>

        <div class="wrap">

          <header>
            <h2><?php echo $cat->name; ?></h2>
          </header>

          <div class="content">

            <?php

              $posts_array = get_posts(
                array(
                    'posts_per_page' => -1,
                    'post_type' => 'services',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'servicestype',
                            'field' => 'term_id',
                            'terms' => $cat->term_id,
                        )
                    )
                )
              );

              foreach ( $posts_array as $post ) :

                $link = get_permalink($post->ID);

            ?>

              <div class="item-services">
                <a href="<?php echo $link; ?>">
                  <header>
                    <h3><?php echo $post->post_title; ?></h3>
                  </header>
                  <div class="content">
                    <?php echo wpautop($post->post_content); ?>
                  </div>
                  <footer class="clearfix">
                    <span class="button white pull-right">En savoir plus</span>
                  </footer>
                </a>
              </div>

            <?php
              endforeach;
            ?>

          </div>

        </div>

    </article>

  <?php

    endforeach;

  ?>
</section>
