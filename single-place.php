<?php

  $placeTitle           = $post->post_title ;
  $placeContent         = $post->post_content;

  $placeImageCover      = get_field('place_image_cover');

  $placeGallery         = get_field('place_gallery');

?>

<section id="event-single">

  <header>

    <div class="image-wrapper">
      <img class="fill fittable" src="<?php echo $placeImageCover['url']; ?>" alt="<?php echo $eventTitle; ?>">
    </div>

    <div class="container title-content">

      <div class="row">

        <div class="col-sm-6 col-xs-12">
          <h1><?php echo $placeTitle; ?></h1>

        </div>

      </div>

    </div>

  </header>

  <article id="content" class="container">

    <div class="content margin-bottom-large col-lg-6 col-xs-12">
      <?php echo wpautop($placeContent); ?>
    </div>

    <div class="information margin-bottom-large col-lg-6 col-xs-12">

      <h2><?php _e('Informations pratiques','moulinseventeen'); ?></h2>

    </div>

  </article>

  <?php if( $placeGallery ): ?>

    <article class="container margin-bottom-large">
      <h2><?php _e('En photos','moulinseventeen'); ?></h2>
      <div id="slider" class="flexslider">
        <ul class="slides">
            <?php foreach( $placeGallery as $image ): ?>
                <li>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <p><?php echo $image['caption']; ?></p>
                </li>
            <?php endforeach; ?>
        </ul>
      </div>
      <div id="carousel" class="flexslider">
          <ul class="slides">
              <?php foreach( $placeGallery as $image ): ?>
                  <li>
                      <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                  </li>
              <?php endforeach; ?>
          </ul>
      </div>
    </article>

<?php endif; ?>



  <?php
    $queryAgrs = array(
      'post_type'=>'place',
      'posts_per_page'=>-1,
      'suppress_filters' => false
    );

    $placeGridItems = get_posts ($queryAgrs);
    if (!empty($placeGridItems)):
  ?>

    <article id="place" class="container">
      <h2><?php _e('D\'autres salles','moulinseventeen'); ?></h2>
      <?php
        set_query_var( 'placeGridItems', $placeGridItems);
        get_template_part('templates/place-grid');
      ?>
    </article>

  <?php endif; ?>

</section>

<script type="text/javascript" charset="utf-8">
  $(window).load(function() {
    $('.flexslider').flexslider();
  });
</script>

<script>

  jQuery(document).ready(
    function(){
      jQuery(".fittable").fit()

    }

  )
</script>
