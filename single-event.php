<?php

  $eventTitle                 = $post->post_title ;
  $eventContent               = $post->post_content;

  $eventImageCover            = get_field('event_image_cover');
  $eventPlace                 = get_field('event_place');
  $eventHours                = get_field('event_hours');

?>

<header class="container padding-top-medium padding-bottom-medium">

  <h1><?php echo $eventTitle; ?></h1>

</header>

<?php if(!empty($contentFull)): ?>

  <section class="container margin-top-<?php echo  $margin['fc_space_grp_top'];?> margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">

    <div class="content content__paragraph">
      <?php echo $content; ?>
    </div>

  </section>

<?php endif; ?>

<?php get_template_part('templates/flexible-content');?>

<section id="event-single" class="wrap-global-content">

  <!-- SECTION EVENTS -->
  <?php
    $queryAgrs = array(
      'post_type'         => 'event',
      'posts_per_page'    => 3,
      'orderby'           => 'title',
      'order'             => 'DESC',
      'suppress_filters'  => false
    );

    $eventGridItems = get_posts ($queryAgrs);
    if (!empty($eventGridItems)):
  ?>

  <article id="event" class="container margin-bottom-large">

    <h2><?php _e('D\'autres événements','moulinseventeen'); ?></h2>

    <div class="event-grid clearfix">

      <?php
        set_query_var( 'eventGridItems', $eventGridItems);
        get_template_part('templates/event-grid');
      ?>

    </div>

  </article>

  <?php endif; ?>

  <footer>
    <div class="container clearfix">
      <a class="button pull-right" href="<?php echo get_post_type_archive_link( 'event' ); ?>"> < <?php _e('Retour à la liste des événements','moulinseventeen'); ?></a>
    </div>
  </footer>

</section>
