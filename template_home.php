<?php
/**
 * Template Name: Template Home
 */
?>

<!-- SECTION SLIDESHOW -->
<?php
  $queryAgrs = array(
    'post_type'=>'slideshow',
    'posts_per_page'=>-1,
    'suppress_filters' => false
  );

  $slideshowGridItems = get_posts ($queryAgrs);
  if (!empty($slideshowGridItems)):
?>

<section class="slideshow">

  <div id="slider" class="slider-wrapper">

    <div class="cd-hero">
      <ul class="cd-hero-slider autoplay">

        <?php
          set_query_var( 'slideshowGridItems', $slideshowGridItems);
          get_template_part('templates/slider');
        ?>

      </ul>

      <ul class="cd-slider-arrows">
        <li><a href="#0" class="next-slide">Next</a></li>
        <li><a href="#0" class="prev-slide">Prev</a></li>
      </ul>

    </div>

  </div>

</section>

<?php
  endif;
?>

<!-- SECTION SERVICES -->
<section class="hilight-services-home container">

  <header>
    <header>
      <h2><?php _e('Des activités pour tous ...','moulinseventeen'); ?></h2>
    </header>
  </header>

  <div class="content row">

    <?php
      $args = array(
        'type'                     => 'services',
        'orderby'                  => 'ID',
        'order'                    => 'ASC',
        'hide_empty'               => 1,
        'hierarchical'             => 1,
        'taxonomy'                 => 'servicestype',
        'pad_counts'               => false
      );

      $categories = get_categories( $args );

      foreach ( $categories as $cat ) :

        $color            = get_field('tax_services_color', $cat);
        $image            = get_field('tax_services_image_bg', $cat);
        $link             = get_term_link($cat->term_id);

    ?>

      <article class="col-sm-4 col-xs-12 animated fade-to-top">

        <a href="<?php echo $link; ?>">

          <div class="image-wrapper" style="background: url('<?php echo $image['url'];?>') no-repeat center/cover;"></div>

          <header>
            <h3><?php echo $cat->name; ?></h3>
          </header>

        </a>

      </article>

    <?php

      endforeach;

    ?>

  </div>

</section>

<!-- SECTION NEWS -->
<?php

  $queryAgrs = array(
    'post_type'         => 'post',
    'posts_per_page'    => 3,
    'suppress_filters'  => false
  );

  $postGridItems = new WP_Query( $queryAgrs );

	if ( $postGridItems->have_posts() ) :
?>

<section class="news-grid container">

  <header class="header-tag animated fade-to-top">
    <h2><?php _e('Notre actualité','kiff-starter-theme'); ?></h2>
  </header>

  <div class="content">

    <div class="row">

      <?php
  			while ( $postGridItems->have_posts() ) : $postGridItems->the_post();
          set_query_var( 'postGridItems', $postGridItems);
          get_template_part('templates/news-grid');
  			endwhile;

        wp_reset_query();
      ?>

    </div>

  </div>

  <footer class="text-right margin-top-medium">
    <a href="<?php echo get_page_link(451); ?>" class="button light-grey"><?php _e('Voir plus de news','kiff-starter-theme'); ?></a>
  </footer>

</section>

<?php endif; ?>

<!-- SECTION EVENTS -->
<?php
  $date = date("Y-m-d");
  $queryAgrs = array(
    'post_type'=>'event',
    'posts_per_page'=>-1,
    'suppress_filters' => false,
    'meta_query'	=> array(
      'relation'		=> 'OR',
      array(
        'key'	 	=> 'event_date_start',
        'value'	  	=> $date,
        'compare' 	=> '>',
      ),
      array(
        'key'	  	=> ' event_date_end',
        'value'	  	=> $date,
        'compare' 	=> '>',
      ),
    ),
  );

  $eventGridItems = get_posts ($queryAgrs);
  if (!empty($eventGridItems)):
?>

  <section id="event" class="container">

    <header class="text-center padding-top-medium padding-bottom-medium">
      <h2><?php _e('Les événements','moulinseventeen'); ?></h2>
    </header>

    <article class="event-grid first-big">

      <?php
        set_query_var( 'eventGridItems', $eventGridItems);
        get_template_part('templates/event-grid');
      ?>

    </article>

  </section>

<?php
  endif;
?>
