<!-- SECTION EVENTS -->
<?php
  $date = date("Y-m-d");
  $queryAgrs = array(
    'post_type'=>'event',
    'posts_per_page'=>-1,
    'suppress_filters' => false,
    'meta_query'	=> array(
      'relation'		=> 'OR',
      array(
        'key'	 	=> 'event_date_start',
        'value'	  	=> $date,
        'compare' 	=> '>',
      ),
      array(
        'key'	  	=> ' event_date_end',
        'value'	  	=> $date,
        'compare' 	=> '>',
      ),
    ),
  );

  $eventGridItems = get_posts ($queryAgrs);
  if (!empty($eventGridItems)):
?>

  <section id="event" class="container">

    <header class="text-center padding-top-medium padding-bottom-medium">
      <h1><?php _e('Les événements','moulinseventeen'); ?></h1>
    </header>

    <article class="event-grid first-big clearfix">

      <?php
        set_query_var( 'eventGridItems', $eventGridItems);
        get_template_part('templates/event-grid');
      ?>

    </article>

  </section>

<?php
  else:
?>

<section id="event" class="container">

  <header class="text-center padding-top-medium padding-bottom-medium">
    <h1><?php _e('Les événements','moulinseventeen'); ?></h1>
  </header>

  <article class="event-grid first-big clearfix">

    <p><strong>Il n'y a pas d'événement prévu pour le moment ...</strong></p>

  </article>

</section>


<?php
  endif;
?>

<script>
  jQuery(document).ready(
    function(){
      jQuery(".fittable").fit()

    }

  )
</script>
