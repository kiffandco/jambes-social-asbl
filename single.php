<div class="page-wrapper">
  <?php while (have_posts()) : the_post();

    $contentTitle           = get_the_title();
    $contentFull            = get_the_content();

    $content                = apply_filters( 'the_content', $contentFull );
    $content                = wpautop($content,false);
    $content                = shortcode_unautop($content);

    $headerImage            = get_the_post_thumbnail( $page_for_posts, 'full' );

  ?>

    <?php if(!empty($headerImage)):

    ?>

      <section class="header-page ">
        <div class="js-replace-img">
          <?php echo $headerImage; ?>
        </div>
      </section>

    <?php endif; ?>

    <header class="text-center padding-top-medium padding-bottom-medium">

      <h1><?php echo $contentTitle; ?></h1>

    </header>

    <?php if(!empty($contentFull)): ?>

      <section class="animated container fade-to-top margin-top-<?php echo  $margin['fc_space_grp_top'];?> margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">

        <div class="content animated fade-to-top content__paragraph">
          <?php echo $content; ?>
        </div>

      </section>

    <?php endif; ?>

    <?php //get_template_part('templates/flexible-content');?>

  <?php endwhile; ?>

  <!-- SECTION NEWS -->
  <?php
    $queryAgrs = array(
      'post_type'           => 'post',
      'posts_per_page'      => 3,
      'suppress_filters'    => false,
      'orderby'             => 'rand',
      'post__not_in'        => array( get_the_id() )
    );

    $postGridItems = new WP_Query( $queryAgrs );

  	if ( $postGridItems->have_posts() ) :
  ?>

    <section class="news-grid margin-bottom-medium">

      <div class="container">

        <header class="header-tag margin-top-medium margin-bottom-medium animated fade-to-top">
          <h2><?php _e('Autres news','kiff-starter-theme'); ?></h2>
        </header>

        <div class="content row">

          <?php
      			while ( $postGridItems->have_posts() ) : $postGridItems->the_post();
              set_query_var( 'postGridItems', $postGridItems);
              get_template_part('templates/news-grid');
      			endwhile;

            wp_reset_query();
          ?>

        </div>

      </div>

    </section>

  <?php endif; ?>

  <?php get_template_part('templates/flexible-content');?>


</div>
