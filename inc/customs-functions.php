<?php
// JQUERY
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "https://code.jquery.com/jquery-3.5.1.min.js", false, null);
   wp_enqueue_script('jquery');
}

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyCcswz3vCfIEu0UAkwNJiTxKB-PYOYFGxw');
}

add_action('acf/init', 'my_acf_init');

?>
