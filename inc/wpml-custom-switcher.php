<?php
function wpml_custom_switcher(){

	$output.= '<div id="language-selector">';
	$output.='<a href="#" class="language-selector-trigger">'.ICL_LANGUAGE_CODE.'</a>';
	$output.= '<ul>';
		$languages = icl_get_languages('skip_missing=1');
		$langs = [];
		if (count($languages)<=1) return ;
		foreach($languages as $l){
	    	if (!$l['active'])$langs[] = '<li><a href="'.$l['url'].'">'.$l['language_code'].'</a></li>';
		}
		$output.= join(', ', $langs);
		$output.= '</ul>';
	$output.= '</div>';
	echo $output;

}
add_action('wpml_custom_switcher','wpml_custom_switcher')
?>
