<?php

	function warp_youtube_player ($sourceStr){

		if (empty($sourceStr))return '';
		 $doc = new DOMDocument();
		 $docContent = mb_convert_encoding($sourceStr, 'HTML-ENTITIES', "UTF-8");
		 $doc->encoding = 'UTF-8';
		 $doc->loadHTML($docContent);
		 $selector = new DOMXPath($doc);
		 $result = $selector->query('//iframe');
		 foreach ($result as $key => $iframe) {
			 $element = $doc->createElement('div');
			 $element->setAttribute('class','yt-warper');
			 $iframe->parentNode->replaceChild ($element,$iframe);
			 $element->appendChild($iframe);
			 $doc->appendChild($element);
		}
		$sourceStr = $doc->saveHTML();

		return $sourceStr;

	}
	add_filter('warp_youtube_player','warp_youtube_player');


	function excerpt_content ($post,$addLink,$limits=150){

		$link = "";
		if ($addLink){

			$link = '<br><a class="button" href="'.get_page_link($post->ID).'">Lire la suite</a>';

		}
		if (!empty($post->post_content)){
				return '<p>'.substr(strip_tags($post->post_content),0,$limits).' ...</p>'.$link;
		}else {

			return "";
		}


	}
	add_filter('excerpt_content','excerpt_content',10,3);



 ?>
