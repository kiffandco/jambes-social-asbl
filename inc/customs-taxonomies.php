<?php
function register_services_type (){

	$args = array(
		'hierarchical'      => true,
		'label'            	=> "Les types de services",
		'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
		'rewrite' => array('slug' => 'nos-services'),
	  );

	  register_taxonomy( 'servicestype', array('services'), $args );

}

function regiterTaxonomies (){

	register_services_type ();

}

add_action('init','regiterTaxonomies',100);

?>
