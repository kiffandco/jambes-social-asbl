<?php

function register_slideshow(){

	$CTPArgs = array(
		'public' => true,
		'label'  => 'Slideshow',
		'has_archive' => true,
		'rewrite' => array('slug' => 'slideshow'),

	);

	register_post_type('slideshow',$CTPArgs);

}

function register_services(){

	$CTPArgs = array(
		'public' => true,
		'label'  => 'Services',
		'has_archive' => true,
		'rewrite' => array('slug' => 'services'),

	);

	register_post_type('services',$CTPArgs);

}

function register_event(){

	$CTPArgs = array(
		'public' => true,
		'label'  => 'Evénements',
		'has_archive' => true,
		'rewrite' => array('slug' => 'event'),

	);

	register_post_type('event',$CTPArgs);

}

	function register_post_types (){

		register_services();
		register_slideshow();
		register_event();


	}

	add_action( 'init', 'register_post_types' );


 ?>
