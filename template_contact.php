<?php
/**
 * Template Name: Template Contact
 */
?>

<?php
  while (have_posts()) : the_post();

  $cover = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  $content = apply_filters( 'the_content', $post->post_content );
	$content = wpautop($content,false);
	$content = shortcode_unautop($content);

?>

<section id="wrap-global-content">

  <div class="container">

    <header>
      <h1><?php the_title(); ?></h1>
    </header>

  </div>

</section>

<?php endwhile; ?>

<script>
  jQuery(document).ready(
    function(){
      jQuery(".fittable").fit()

    }

  )
</script>
