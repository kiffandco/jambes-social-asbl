<!-- SECTION NEWS -->
<?php

  $queryAgrs = array(
    'post_type'         => 'post',
    'posts_per_page'    => -1,
    'suppress_filters'  => false
  );

  $postGridItems = new WP_Query( $queryAgrs );

	if ( $postGridItems->have_posts() ) :
?>

<section class="news-grid container">

  <header class="text-center padding-top-medium padding-bottom-medium">
    <h1><?php _e('Notre actualité','kiff-starter-theme'); ?></h1>
  </header>

  <div class="content">

    <div class="row">

      <?php
  			while ( $postGridItems->have_posts() ) : $postGridItems->the_post();
          set_query_var( 'postGridItems', $postGridItems);
          get_template_part('templates/news-grid');
  			endwhile;

        wp_reset_query();
      ?>

    </div>

  </div>

</section>

<?php endif; ?>
