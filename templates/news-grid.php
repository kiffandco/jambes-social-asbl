<?php

  $postTitle              = get_the_title();
  $postImage              = get_the_post_thumbnail( get_the_id(), 'full' );
  $postAbstract           = get_the_excerpt();

  $postLinkPage           = get_page_link(get_the_id());

?>

<article class="news-item <?php echo $format; ?> col-md-4 col-sm-12 animated fade-to-top">

  <a href="<?php echo $postLinkPage; ?>">

    <div class="js-replace-img">
      <?php echo $postImage; ?>
    </div>

    <header>
      <span></span>
      <h3><?php echo $postTitle; ?></h3>
    </header>

    <div class="content">
      <?php echo wpautop($postAbstract); ?>
    </div>

    <footer class="text-right">
      <span class="button"><?php _e('Plus d\'infos','kiff-starter-theme'); ?></span>
    </footer>

  </a>

</article>
