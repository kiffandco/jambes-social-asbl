<div class="colophon clearfix container">

	<div class="navColophon"><p><?php echo get_bloginfo(); ?>  © <?php echo date('Y'); ?></p>

		<?php
				if (has_nav_menu('colophon_navigation')) :
						wp_nav_menu(['theme_location' => 'colophon_navigation', 'menu_class' => 'navbar-nav-colophon','container'=>'',]);
				endif;
		?>

	</div>

	<a class="kiff-logo" href="http://www.kiffandco.be" target="_blank">
		created by KIFF.
	</a>

</div>
