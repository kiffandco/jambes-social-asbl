<article class="event-grid clearfix">
<?php


  foreach ($placeGridItems as $item) :

    $placeTitle					  = $item->post_title;
    $placeContent				  = $item->post_content;
    $placeImageList   		= get_field('place_image_list',$item->ID);
    $placeLink            = get_page_link($item->ID);

?>

<figure class="col-sm-3 col-xs-12">

  <a href="<?php echo $placeLink; ?>">

    <div class="image-wrapper">
      <img class="fill fittable" src="<?php echo $placeImageList['url']; ?>" alt="<?php echo $placeTitle; ?>">
    </div>

    <figcaption>
      <h3><?php echo $placeTitle; ?></h3>
    </figcaption>

    <div class="more">
      <span class="button white"><?php _e('Plus d\'infos','moulinseventeen'); ?></span>
    </div>

  </a>

</figure>

<?php

  $classCount ++;
  endforeach;

?>

</article>
