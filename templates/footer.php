<footer id="main-footer">

  <div class="container">

    <?php get_template_part('templates/community'); ?>

    <a class="brand-logo-footer" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>

    <?php
        if (has_nav_menu('footer_navigation')) :
            wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'navbar-nav-footer','container'=>'',]);
        endif;
    ?>

    <?php get_template_part('templates/colophon'); ?>

  </div>

</footer>
