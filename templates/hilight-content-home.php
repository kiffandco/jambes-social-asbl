<section class="hilight-content-home">
<?php

  foreach ($hilightContentHomeGridItems as $item) :

    $contentTitle					  = $item->post_title;
    $contentContent				  = $item->post_content;
    $contentImageCover   		= get_field('hilight_content_home_image_cover',$item->ID);

    $contentImageBg   		  = get_field('hilight_content_home_image_background',$item->ID);
    $contentImageTop  		  = get_field('hilight_content_home_image_top',$item->ID);

    $contentSelected1  		  = get_field('hilight_content_home_selected_content_1',$item->ID);
    $contentSelected2  		  = get_field('hilight_content_home_selected_content_2',$item->ID);
    $contentSelectedlabel2  = get_field('hilight_content_home_selected_content_label',$item->ID);



    if(empty($contentImageCover)):
      $class = "item";
    else:
      $class = "item full-width";
    endif;

?>

<?php

  if(empty($contentImageCover)):

?>

  <article class="<?php echo $class; ?>">

    <div class="container">

      <div class="col-sm-5 col-xs-12">
        <div class="image-wrapper">
          <img class="fill fittable" src="<?php echo $contentImageBg['url']; ?>" alt="<?php echo $contentTitle;?>">
        </div>
        <div class="image-top">
          <img class="" src="<?php echo $contentImageTop['url']; ?>" alt="<?php echo $contentTitle;?>">
        </div>
      </div>

      <div class="col-sm-6 col-xs-12 col-sm-offset-1">

        <header>
          <h2><?php echo $contentTitle; ?></h2>
        </header>

        <div class="content">
          <?php echo wpautop($contentContent); ?>
        </div>

        <footer>
          <?php if(!empty($contentSelected2)): ?>
            <a class="button yellow col-sm-6 col-xs-12 pull-right" href="<?php echo get_page_link($contentSelected2->ID); ?>"><?php echo $contentSelectedlabel2; ?></a>
          <?php endif; ?>
          <a class="button light-grey pull-right col-sm-6 col-xs-12" href="<?php echo get_page_link($contentSelected1->ID); ?>"><?php _e('En savoir plus','moulinseventeen'); ?></a>
        </footer>

      </div>

    </div>

  </article>


<?php
  else:
?>

  <article class="<?php echo $class; ?>" style="background: url('<?php echo $contentImageCover['url'];?>') no-repeat center/cover;">

    <div class="container">

      <header>
        <h2><?php echo $contentTitle; ?></h2>
      </header>

      <div class="content">
        <?php echo wpautop($contentContent); ?>
      </div>

      <footer class="clearfix">
        <?php if(!empty($contentSelected2)): ?>
          <a class="button yellow col-sm-3 col-xs-12 pull-right" href="<?php echo get_page_link($contentSelected2->ID); ?>"><?php echo $contentSelectedlabel2; ?></a>
        <?php endif; ?>
        <a class="button white col-sm-3 col-xs-12 pull-right" href="<?php echo get_page_link($contentSelected1->ID); ?>"><?php _e('En savoir plus','moulinseventeen'); ?></a>
      </footer>

    </div>

  </article>

<?php
  endif;
?>

<?php
  endforeach;
?>

</section>
