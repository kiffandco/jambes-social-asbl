<?php

	$classCount             = "0";
	foreach ($slideshowGridItems as $item):

  $slideshowTitle         = $item->post_title;
  $slideshowTag       		= get_field('slidshow_texte_top',$item);
	$slideshowImage        	= get_field('slideshow_image',$item);
	$slideshowImageAlign    = get_field('slideshow_alignement_image',$item);
	$slideshowTextAlign    	= get_field('slideshow_alignement_text',$item);
	$slideshowBackground    = get_field('slideshow_background',$item);
  $slideshowLinkPage      = get_field('slideshow_link',$item);

?>

<?php if($classCount == "0"): ?>
	<li class="selected" style="background: <?php echo $slideshowBackground; ?>">
<?php else: ?>
	<li style="background: <?php echo $slideshowBackground; ?>">
<?php endif; ?>

	<a href="<?php echo $slideshowLinkPage['url']; ?>">

		<?php if($slideshowImageAlign == 'left'): ?>

			<div class="image-wrapper js-replace-img left col-sm-6 col-12">
				<img src="<?php echo $slideshowImage['url']; ?>" alt="<?php echo $slideshowTitle; ?>">
			</div>

		<?php elseif($slideshowImageAlign == 'right'): ?>

			<div class="image-wrapper js-replace-img right col-sm-6 offset-sm-6 col-12">
				<img src="<?php echo $slideshowImage['url']; ?>" alt="<?php echo $slideshowTitle; ?>">
			</div>

		<?php elseif($slideshowImageAlign == 'center'): ?>

			<div class="image-wrapper js-replace-img center col-sm-6 offset-sm-3 col-12">
				<img src="<?php echo $slideshowImage['url']; ?>" alt="<?php echo $slideshowTitle; ?>">
			</div>

		<?php else: ?>

			<div class="js-replace-img">
				<img src="<?php echo $slideshowImage['url']; ?>" alt="<?php echo $slideshowTitle; ?>">
			</div>

		<?php endif; ?>

		<figcaption class="<?php echo $slideshowTextAlign; ?>">

			<header>
				<span><?php echo $slideshowTag; ?></span>
			</header>

			<div class="content">
				<h2><?php echo $slideshowTitle; ?></h2>
			</div>

			<footer class="clearfix">
				<span class="button yellow pull-right"><?php echo $slideshowLinkPage['title']; ?></span>
			</footer>

		</figcaption>

	</a>

</li>

<?php

	$classCount ++;
	endforeach;

?>
