<?php

  // FLEXIBLE CONTENT LOOP
  if( have_rows('flexible_content') ):
     // loop through the rows of data
    while ( have_rows('flexible_content') ) : the_row();

        // CONTENT FULL 100%
        if ( get_row_layout() == 'flexible_content_content_full' ):

            $contentFull           = get_sub_field('fc_content_full');
            $contentFullBackground = get_sub_field('fc_content_full_background');
            $margin                = get_sub_field('fc_space_grp');


            if($contentFullBackground == "white"): ?>
            <section class="container animated fade-to-top margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
              <div class="white-box ">

          <?php else: ?>

            <section class="container animated fade-to-top margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">

          <?php endif; ?>

              <div class="content animated fade-to-top content__paragraph">
                <?php echo wpautop($contentFull); ?>
              </div>

          <?php if($contentFullBackground == "white"): ?>
            </div>
          <?php endif; ?>

            </section>

        <?php
        // CONTENT FULL IMAGE
        elseif( get_row_layout() == 'flexible_content_image_full' ):

        	$contentFullImage      = get_sub_field('fc_content_image_100');
          $contentFullImageStyle = get_sub_field('fc_content_image_100_style');
          $contentFullImageSize  = get_sub_field('fc_content_image_100_size');
          $contentFullImageAlign = get_sub_field('fc_content_image_100_alignement');
          $margin                = get_sub_field('fc_space_grp');

        ?>

        <section class="animated fade-to-top no-padding margin-top-<?php echo $margin['fc_space_grp_top'];?> margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
          <?php if($contentFullImageStyle == "full"): ?>
            <div class="image-100 <?php echo $contentFullImageSize; ?> js-replace-img">
          <?php else: ?>
            <div class="image-100 text-<?php echo $contentFullImageAlign; ?> container">
          <?php endif; ?>
                <img src="<?php echo $contentFullImage['url']; ?>" alt="<?php echo $contentFullImage['title']; ?>">
            </div>
        </section>

        <?php
        // CONTENT GALLERY
        elseif( get_row_layout() == 'flexible_content_gallery' ):

          $contentImages    = get_sub_field('flexible_content_gallery_images');
          $margin           = get_sub_field('fc_space_grp');
        ?>
        <section class="container animated fade-to-top no-padding  margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
          <div id="gallery-slide" class="d-flex flex-wrap">
          <?php foreach ($contentImages as $image):?>

            <div class="col-md-3 col-sm-4 col-12 fade-to-pop">
              <a class="grouped_elements js-replace-img animated " data-fancybox="groupFancyBox"  rel="groupFancyBox"  href="<?php echo $image['url']; ?>">
                <img width="100%" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['title']; ?>">
              </a>
            </div>

          <?php endforeach;?>
          </div>
        </section>

        <?php
        // CONTENT 2 COLONNES
        elseif( get_row_layout() == 'flexible_content_2colonne' ):

        	$contentColonne        = get_sub_field('fc_content_2colonne');
          $contentFullBackground = get_sub_field('fc_content_2colonne_background');
          $margin                = get_sub_field('fc_space_grp');


          if($contentFullBackground == "white"): ?>
          <section class="container animated fade-to-top margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
            <div class="white-box">

        <?php else: ?>

          <section class="container animated fade-to-top margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">

        <?php endif; ?>

            <div class=" collumn-text content__paragraph">
              <?php echo wpautop($contentColonne); ?>
            </div>

            <?php if($contentFullBackground == "white"): ?>
              </div>
            <?php endif; ?>

          </section>

          <?php
          // VIDEO
          elseif (get_row_layout()== 'flexible_content_video') :
            if( have_rows('fc_content_video_repeater') ):
              $margin           = get_sub_field('fc_space_grp');
          ?>
            <section class="container animated fade-to-top video-grid margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
              <?php while ( have_rows('fc_content_video_repeater') ) : the_row();
              $videoLink = get_sub_field('fc_content_video_repeater_url');?>
              <div class="video-grid-item">
                <div class="embed-responsive embed-responsive-16by9">
                  <?php echo $videoLink;?>
                </div>
              </div>
          <?php endwhile;?>
            </section>

        <?php endif;?>

        <?php
        // CONTENT 1/1
        elseif( get_row_layout() == 'flexible_content_image_half' ):

        	$contentHalfText           = get_sub_field('fc_content_half_text');
          $contentHalfImage          = get_sub_field('fc_content_half_image');
          $contentHalfAlignement     = get_sub_field('fc_content_half_alignement');
          $contentHalfStyleImage     = get_sub_field('fc_content_half_style_img');
          $contentHalfLink           = get_sub_field('fc_content_half_link');
          $contentHalfLinkUrl        = get_permalink($contentHalfLink);
          $margin                    = get_sub_field('fc_space_grp');

          if($contentHalfAlignement == 'left'):

            $align  = "align-image-left";

          else:

            $align  = "align-image-right";

          endif;

        $contentFullBackground = get_sub_field('fc_content_half_background');
        if($contentFullBackground == "white"): ?>
          <section class="container clearfix text-image <?php echo $align ;?> animated fade-to-top margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
            <div class="white-box ">
        <?php else: ?>
          <section class="container animated clearfix text-image <?php echo $align ;?> fade-to-top margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
        <?php endif; ?>
          <div class="row">
            <div class="text content__paragraph col-md-6 col-sm-12 animated fade-to-top">
                <?php echo wpautop($contentHalfText); ?>

                <?php if(!empty($contentHalfLink)): ?>
                  <footer>
                    <a class="btn-theme" href="<?php echo $contentHalfLinkUrl; ?>"><?php _e('En savoir plus','kiff-starter-theme'); ?></a>
                  </footer>
                <?php endif; ?>
            </div>
            <div class="image-container col-md-6 col-sm-12 animated fade-to-top">
              <?php if($contentHalfStyleImage == "cover"): ?>
                <div class="js-replace-img">
              <?php else: ?>
                <div class="normal-image">
              <?php endif; ?>
                <img src="<?php echo $contentHalfImage['url']; ?>" alt="image">
              </div>
            </div>
          </div>

          <?php         if($contentFullBackground == "white"): ?>
            </div>
          <?php endif; ?>

        </section>

          <?php


          // QUOTE
          elseif( get_row_layout() == 'flexible_content_quote' ):

          	$contentQuote     = get_sub_field('fc_content_quote_blockquot');
            $authors          = get_sub_field('fc_content_quote_authors');
            $quoteLink        = get_sub_field('fc_content_quote_link');
            $quoteRole        = get_sub_field('fc_content_quote_role');
            $quoteImage       = get_sub_field('fc_content_quote_picture');
            $margin           = get_sub_field('fc_space_grp');
          ?>

          <section class="container animated fade-to-top clearfix margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">
            <figure class="quote">
              <blockquote cite="<?php echo $quoteLink?>">
                <?php  echo $contentQuote; ?>
              </blockquote>
              <div class="caption">
                <?php if ($quoteImage):?>
                <div class="js-replace-img">
                  <img src="<?php echo $quoteImage['url']; ?>" alt="image">
                </div>
                <?php endif;?>
                <div class="content">
                  <p><?php echo $authors ?></p>
                  <p><?php echo $quoteRole ?></p>
                </div>
              </div>
            </figure>
          </section>

          <?php


          // ACCORDION
          elseif( get_row_layout() == 'flexible_content_accordeon' ):
            $margin           = get_sub_field('fc_space_grp');
          ?>

          <section class="container animated fade-to-top clearfix margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">

            <?php if( have_rows('flexible_content_accordeon_item') ): ?>

            	<div class="accordion" id="accordionFlexible">

            	<?php $i = "0"; while( have_rows('flexible_content_accordeon_item') ): the_row();

            		// vars
            		$title      = get_sub_field('flexible_content_accordeon_item_title');
            		$content    = get_sub_field('flexible_content_accordeon_content');

            		?>

                <div class="card">
                  <div class="card-header" id="heading<?php echo $i; ?>">
                    <h2 class="mb-0">
                      <?php if($i == "0"): ?>
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                      <?php else: ?>
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                      <?php endif; ?>
                        <?php echo $title; ?>
                      </button>
                    </h2>
                  </div>

                  <?php if($i == "0"): ?>
                    <div id="collapse<?php echo $i; ?>" class="collapse show" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordionFlexible">
                  <?php else: ?>
                    <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordionFlexible">
                  <?php endif; ?>

                    <div class="card-body">
                      <?php echo wpautop($content); ?>
                    </div>
                  </div>
                </div>

            	<?php $i++; endwhile; ?>

              </div>

            <?php endif; ?>

          </section>

        <?php
        // LINK
        elseif( get_row_layout() == 'flexible_content_link' ):
          $button_repeater  = get_sub_field('flexible_content_link_button_elements');
          $margin           = get_sub_field('fc_space_grp');
        ?>

          <section class="container text-center margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?> relative-position">
            <footer>
              <?php if ($button_repeater):
                  foreach ($button_repeater as $el):
                    ?>
                    <span><a class="btn-theme margin-right-base" href="<?php echo $el['flexible_content_link_button_elements_link']['url'];?>"><?php echo $el['flexible_content_link_button_elements_label']?></a></span>
                  <?php endforeach;
                ?>
              <?php endif;?>
            </footer>
          </section>

          <?php
            // DOWNLOAD
            elseif( get_row_layout() == 'flexible_content_download' ):
              $linkUrlAndTitle    = get_sub_field('flexible_content_download_link');
              $buttonLabel        = get_sub_field('flexible_content_download_label');
              $margin             = get_sub_field('fc_space_grp');
          ?>
              <section class="container text-center margin-top-<?php echo $margin['fc_space_grp_top'];?>  margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?> relative-position">
                <a class="btn-theme" download href="<?php echo $linkUrlAndTitle['url'];?>"><?php echo $buttonLabel;?></a>
              </section>

          <?php
            // TEAM
            elseif( get_row_layout() == 'flexible_content_team' ):

              if( have_rows('fc_team_repeater') ):
          ?>

            <section class="team container">
              <div class="row">

                <?php while( have_rows('fc_team_repeater') ): the_row();

                  // vars
                  $name             = get_sub_field('fc_team_repeater_name');
                  $functionTeam     = get_sub_field('fc_team_repeater_function');
                  $email            = get_sub_field('fc_team_repeater_email');
                  $text             = get_sub_field('fc_team_repeater_text');
                  $photo            = get_sub_field('fc_team_repeater_photo');

                ?>

                <article class="item col-md-4 col-sm-6 col-xs-12 animated fade-to-top">

                  <div class="image-wrapper js-replace-img">
                    <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['title']; ?>">
                  </div>

                  <header>
                    <h3><?php echo $name; ?></h3>
                    <?php if(!empty($functionTeam)): ?>
                      <h4><?php echo $functionTeam; ?></h4>
                    <?php endif; ?>
                  </header>

                  <div class="content">
                    <?php echo wpautop($text); ?>
                  </div>

                  <?php if(!empty($email)): ?>
                    <footer>
                      <a class="button" href="mailto:<?php echo $email; ?>">Me contacter</a>
                    </footer>
                  <?php endif; ?>

                </article>

                <?php endwhile; ?>

              </div>
            </section>

          <?php endif; ?>


    <?php endif;
    endwhile;


endif;

?>
