<section id="community" class="clearfix">

  <div class="newsletter col-sm-6 col-xs-12">

    <h2><?php _e('Inscription à la newsletter','moulinseventeen'); ?></h2>

    <?php echo do_shortcode('[contact-form-7 id="472" title="Newsletter"]'); ?>

  </div>

  <div class="social col-sm-6 col-xs-12">

    <ul>
      <li><a href="https://www.facebook.com/AsblJambesSocialEtCulturel" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    </ul>

  </div>

</section>
