<nav role="navigation" class="navbar">
    <div class="container">

        <div class="navbar-header">
            <div class="brandContainer col-lg-12 col-md-12 col-xs-12 no-padding">

              <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>

              <div class="navigation">

                <?php
                    if (has_nav_menu('secondary_navigation')) :
                        wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav navbar-nav-secondary','container'=>'',]);
                    endif;
                ?>

                <div class="button_container" id="toggle">
                  <span class="top"></span>
                  <span class="middle"></span>
                  <span class="bottom"></span>
                </div>

                <div class="overlay-nav" id="overlay-nav">
                  <nav class="overlay-menu">
                    <?php
                        if (has_nav_menu('primary_navigation')) :
                            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav','container'=>'',]);
                        endif;
                    ?>
                  </nav>
                </div>

              </div>

            </div>
        </div>
    </div>
</nav>
