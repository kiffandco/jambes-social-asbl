<?php

  $classCount             = "0";

  foreach ($eventGridItems as $item) :

    $eventTitle					  = $item->post_title;
    $eventContent				  = $item->post_content;
    $eventImageList       = get_field('event_image_list',$item->ID);
    $eventLink            = get_page_link($item->ID);

    $eventDateStart       = get_field('event_date_start',$item->ID);
    $eventDateEnd         = get_field('event_date_end',$item->ID);

    if($classCount == "0"):
      $class = "col-sm-6 col-xs-12";
    else:
      $class = "col-sm-3 col-xs-12";
    endif;

?>

<figure class="<?php echo $class; ?>">

  <a href="<?php echo $eventLink; ?>">

    <div class="image-wrapper js-replace-img">
      <img src="<?php echo $eventImageList['url']; ?>" alt="<?php echo $eventImageList['title']; ?>">
    </div>

    <figcaption>
      <?php if(!empty($eventDateEnd)): ?>
        <h4><?php _e('Du','moulinseventeen'); ?> <?php echo $eventDateStart; ?> <?php _e('au','moulinseventeen'); ?> <?php echo $eventDateEnd; ?></h4>
      <?php else: ?>
        <h4><?php echo $eventDateStart; ?></h4>
      <?php endif; ?>
      <h3><?php echo $eventTitle; ?></h3>
    </figcaption>

    <div class="more">
      <span class="button white"><?php _e('Plus d\'infos','moulinseventeen'); ?></span>
    </div>

  </a>

</figure>

<?php

  $classCount ++;
  endforeach;

?>
