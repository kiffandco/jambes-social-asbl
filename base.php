<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <div class="wrap" role="document">

      <?php
        do_action('get_header');
        get_template_part('templates/header');
      ?>

      <div class="content">
        <main>
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->

      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>


    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous" async></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js" async></script>

    <script>

      $(document).ready(function(){
        $('.hilight-services-home .content').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          infinite: true,
          speed: 1000,

          responsive: [

            {
    		      breakpoint: 1280,
    		      settings: {
                dots: false,
                arrows: true,
    		        infinite: true,
    		        centerMode: false,
    		        slidesToShow: 3,
    		        slidesToScroll: 1,
    		      }
    		    },

    		    {
    		      breakpoint: 760,
    		      settings: {
                dots: false,
                arrows: true,
    		        infinite: true,
    		        centerMode: false,
    		        slidesToShow: 1,
    		        slidesToScroll: 1,
    		      }
    		    }
    		  ]

        });

      });

    </script>

  </body>
</html>
