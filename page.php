<div class="page-wrapper">
  <?php while (have_posts()) : the_post();

    $contentTitle           = get_the_title();
    $contentFull            = get_the_content();

    $content                = apply_filters( 'the_content', $contentFull );
    $content                = wpautop($content,false);
    $content                = shortcode_unautop($content);

  ?>

    <header class="text-center padding-top-medium padding-bottom-medium">

      <h1><?php echo $contentTitle; ?></h1>

    </header>

    <?php if(!empty($contentFull)): ?>

      <section class="container margin-top-<?php echo  $margin['fc_space_grp_top'];?> margin-bottom-<?php echo $margin['fc_space_grp_bottom'];?>">

        <div class="content content__paragraph">
          <?php echo $content; ?>
        </div>

      </section>

    <?php endif; ?>

    <?php get_template_part('templates/flexible-content');?>

  <?php endwhile; ?>
</div>
