<?php
/**
 * Template Name: Template Reservation
 */
?>

<?php
  while (have_posts()) : the_post();

  $cover = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  $content = apply_filters( 'the_content', $post->post_content );
	$content = wpautop($content,false);
	$content = shortcode_unautop($content);

?>

<section class="wrap-global-content">

  <div class="container">

    <header>
      <h1><?php the_title(); ?></h1>
    </header>

    <?php
      if (!empty($content)):
    ?>

      <div class="content">
        <?php echo wpautop($content); ?>
      </div>

    <?php
      endif;
    ?>

    <div class="content">
      <?php echo do_shortcode("[ninja_form id=3]"); ?>
    </div>

    <?php

      // check if the repeater field has rows of data
      if( have_rows('repeater_content') ):

        // loop through the rows of data
        while ( have_rows('repeater_content') ) : the_row();

          $title    = get_sub_field('repeater_content_title');
          $content  = get_sub_field('repeater_content_bloc');
          $style    = get_sub_field('repeater_content_style');

    ?>

      <article class="content-wrapper margin-bottom-large item-single <?php echo $style; ?>">

        <?php if(!empty($title)): ?>
          <h2><?php echo $title; ?></h2>
        <?php endif; ?>

        <?php if(!empty($content)): ?>
          <div>
            <?php
              echo $content;
            ?>
          </div>
        <?php endif; ?>

      </article>

    <?php
      endwhile;
      endif;
    ?>

  </div>

</section>

<?php endwhile; ?>

<script>
  jQuery(document).ready(
    function(){
      jQuery(".fittable").fit()

    }

  )
</script>
