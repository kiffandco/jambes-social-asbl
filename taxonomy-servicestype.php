<?php
  $currentId        = get_queried_object()->term_id;
  $color            = get_field('tax_services_color', get_queried_object());
  $image            = get_field('tax_services_image_bg', get_queried_object());

?>

<div class="page-wrapper">

  <div class="container">

    <header class="text-center padding-top-medium padding-bottom-medium">
      <h1><?php echo get_the_title(); ?></h1>
    </header>

  </div>

</div>

<section class="hilight-services container clearfix">

  <article class="item col-xs-12 animated fade-to-top">

    <div class="bg-color" style="background: <?php echo $color; ?>;"></div>
    <div class="image-wrapper" style="background: url('<?php echo $image['url'];?>') no-repeat center/cover;"></div>

      <div class="wrap">

        <div class="content">

          <?php

            $posts_array = get_posts(
              array(
                  'posts_per_page' => -1,
                  'post_type' => 'services',
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'servicestype',
                          'field' => 'term_id',
                          'terms' => $currentId,
                      )
                  )
              )
            );

            foreach ( $posts_array as $post ) :

              $link = get_permalink($post->ID);

          ?>

            <div class="item-services">
              <a href="<?php echo $link; ?>">
                <header>
                  <h3><?php echo $post->post_title; ?></h3>
                </header>
                <div class="content">
                  <?php echo wpautop($post->post_content); ?>
                </div>
                <footer class="clearfix">
                  <span class="button white pull-right">En savoir plus</span>
                </footer>
              </a>
            </div>

          <?php
            endforeach;
          ?>

        </div>

      </div>

  </article>

</section>

<!-- SECTION SERVICES -->
<section class="hilight-services-home container">

  <header>
    <header>
      <h2><?php _e('Il y en a pour tous le monde !','moulinseventeen'); ?></h2>
    </header>
  </header>

  <div class="content row">

    <?php
      $args = array(
        'type'                     => 'services',
        'orderby'                  => 'ID',
        'order'                    => 'ASC',
        'hide_empty'               => 1,
        'hierarchical'             => 1,
        'taxonomy'                 => 'servicestype',
        'pad_counts'               => false
      );

      $categories = get_categories( $args );

      foreach ( $categories as $cat ) :

        $color            = get_field('tax_services_color', $cat);
        $image            = get_field('tax_services_image_bg', $cat);
        $link             = get_term_link($cat->term_id);

    ?>

      <article class="col-sm-4 col-xs-12 animated fade-to-top">

        <a href="<?php echo $link; ?>">

          <div class="image-wrapper" style="background: url('<?php echo $image['url'];?>') no-repeat center/cover;"></div>

          <header>
            <h3><?php echo $cat->name; ?></h3>
          </header>

        </a>

      </article>

    <?php

      endforeach;

    ?>

  </div>

</section>
