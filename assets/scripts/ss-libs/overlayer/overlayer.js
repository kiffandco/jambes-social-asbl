Overlayer = (function (){

		var _instance = null,
			_overlay,
			_overlayContent,
			_overlayCloseControl,
			_hashKey,
			_inited = false,
			_closeDelegateCallback= null,
			_overlayerList
			;


		var Overlayer = function (){

			this.event = {

				OVERLAY_CLOSE:'overlay_close',
				OVERLAY_OPEN:'overlay_open',
				OVERLAY_CONTENT_CHANGE:'overlay_content_change'

			};

			_instance = this;
			_hashKey = new Date().getTime();
			_overlay = document.createElement('DIV');
			_overlayContent = document.createElement('DIV');
			this.overlayContent = _overlayContent;
			_overlayCloseControl = document.createElement('A');
			_overlay.id = 'overlay';
			_overlay.style.display = none;
			_overlayerList = [];
			_overlayContent.addClass('overlay-content');
			_overlayCloseControl.addClass('overlay-close-control');

			_overlay.on('click','.overlay-close-delegate',this.closeOverlay.bind(this));
			_overlayCloseControl.addEventListener('click',this.closeOverlay.bind(this));
			_overlay.appendChild(_overlayContent);
			_overlay.appendChild(_overlayCloseControl);
			document.body.appendChild(_overlay);
			window.addEventListener('keyup',function (e){
				if (e.keyCode === 27 && document.body.hasClass('has-overlay')){
					_instance.closeOverlay();
				}

			}.bind(_instance));
			_inited = true;
			$(document.body).on('click','a[data-overlay-direct-call="true"]',function (e){

				e.preventDefault();
				_dirtyElement = $(e.currentTarget);
				_dirtyElement.data('overlay-content');
				this.setContent($('#'+_dirtyElement.data('overlay-content')));
				_instance.showOverlay();
				this.center();

			}.bind(_instance));

			return this;

		};

		Overlayer.prototype = new SUPERSTRONG.EventDispatcher();
		Overlayer.prototype.constructor = Overlayer;

		var _pOl = Overlayer.prototype;

		_pOl.closeOverlay = function (callback){

			if (!_instance){

				__create();

			}

			if (callback  && typeof callback === 'function') {

				callback.call (_instance);

			}else {

				this.destroy();

			}
			_overlayContent.children().removeClass('in-overlay');
			this.dispatchEvent(SUPERSTRONG.Overlayer.event.OVERLAY_CLOSE);
			return _instance;

		};

		_pOl.getOverlay = function  () {

			if (!_instance){

				__create();

			}

			return _overlay;
		}

		_pOl.setContent = function  (node,clearBefore,showContent){

			if (!_instance){

				__create();

			}

			var _clone = node.clone();
			_clone.attr('data-id',new Date().getTime());

			if (clearBefore){

				_overlayContent.empty()

			}
			_overlayContent.prepend(_clone);


			if (showContent  && typeof showContent === 'function') {

				this.showContent.call (_instance);
				_center();

			}else if (showContent && typeof showContent === 'boolean' ) {

				this.showOverlay();


			}

			_overlayContent.children().addClass('in-overlay');
			this.center();
			this.dispatchEvent(SUPERSTRONG.Overlayer.event.OVERLAY_CONTENT_CHANGE);
			return _instance;

		};

		_pOl.getContent = function  (){


			return _overlayContent.children();

		};

		_pOl.overrideCloseDelegate = function (closeDelegateCallback){

			_closeDelegateCallback = closeDelegateCallback;

		};

		_pOl.showOverlay = function  (){

			if (!_instance){

				__create();

			}

			_overlay.addClass('visible');
			$(document.body).addClass('has-overlay');
			this.dispatchEvent(SUPERSTRONG.Overlayer.event.OVERLAY_OPEN);
			return _instance;

		};

		function __create (){

			_instance = new Overlayer();
			SUPERSTRONG.EventDispatcher.call(_instance);

			return  _instance;

		}

		if (!_instance){
			__create();
		};

		_pOl.center = function(){

			_overlayContent.css({top:(_overlay.height() - _overlayContent.height())*.5});

		};

		_pOl.addClassContent = function (classes){

			this.getContent().addClass(classes);
			this.center();

		}

		_pOl.removeClassContent = function (classes){

			this.getContent().removeClass(classes);
			this.center();

		}

		_pOl.destroy = function (){

			_overlay.removeClass('visible');
			$(document.body).removeClass('has-overlay');
			_overlayContent.children().removeClass('in-overlay');
			_overlayContent.empty();

		};

		return _instance;




})();
