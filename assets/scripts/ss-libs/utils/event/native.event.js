(function (){

	ss.delegate = function (target,event,selector,fn){

		target.addEventListener(event,function (e){

			if (e.target && e.target.matches(selector)){

				fn.call(target,target);

			}


		})

	}

})()
