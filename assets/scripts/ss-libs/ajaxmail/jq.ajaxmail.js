(function ($){

	$.fn.ajaxMail = function (opts){

		var defaults = {

			submitSelector		:'.submit',
			responseSelector	:'#am-responses',
			formWraperSelector	:'#am-form',
			onError			:function (err){},
			onException		:function (data){},
			onSuccess		:function (data){},
			onSubmit		:function (data){},
			onInvalidate	:function (){},

		},
			options
			;
		options = $.extend({},defaults,opts);
		_instance = this,
		isValid = true;

		$(document.body).on('click',options.submitSelector,function (e){
            $elm = $(e.currentTarget);
            _instance.submit($elm.closest('form'));
            e.preventDefault();
         })

		_instance.onError = function (cb){

			options.onError = cb;
			return _instance;

		}

		_instance.onSuccess = function (cb){

			options.onSuccess = cb;
			return _instance;

		}

		_instance.onException = function (cb){

			options.onException = cb;
			return _instance;

		}

		_instance.onSubmit = function (cb){

			options.onSubmit = cb;
			return _instance;

		}

		_instance.onInvalidate = function (cb){

			options.onInvalidate = cb;
			return _instance;

		}

		_instance.invalidate = function (){

			isValid = false;
			return this;

		}

        _instance.submit = function ($form) {
           
           isValid = true;
           options.onSubmit.call(_instance,$form.find('input'));
           if (!isValid){

           		options.onInvalidate.call(_instance,$form.find('input'));
           		return;

           }     
           console.log ($form.serialize());
			$.ajax({
				type 			:$form.attr('method'),
				url 			:$form.attr('action'),
				data 			:$form.serialize(),
				
               	error :function(err) {

					options.onSuccess.call(_instance,err);

               	},
               	success :function(data) {
                    var d = JSON.parse(data);
					if (d.status != "success") {

						options.onException.call(_instance,d);
                            
					} else {
                           
						options.onSuccess.call(_instance,d);
					}

				}
			});
		}  

		return _instance;      
	}

})(jQuery)