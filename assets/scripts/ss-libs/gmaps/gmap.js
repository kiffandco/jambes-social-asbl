GmapController = (function (){

		var _instance;
		var Gmap = function(){

			_instance = this;
			var _map ={},
			_msg =new Date().getTime(),
			_geocoder = new google.maps.Geocoder()
			_polygons= [],
			_markers = []
			;
			function _injectMap (options){

				var options = options || {};
				var opts = {
	                div: options.target || "#map-canvas",
	                lat: options.lat || 50.464422,
	                lng: options.lng || 4.868157999999994,
	                zoom:options.zoom || 17,
	                panControl: options.panControl || false,
	                zoomControl: options.zoomControl || false,
	                scaleControl: options.scaleControl || false,
	                draggable: options.draggable || true,
	                scrollwheel: options.scrollwheel || false,
	                mapTypeId: options.mapTypeId || google.maps.MapTypeId.ROADMAP

	            }

				_instance._map = new gmap(opts);
				SUPERSTRONG.trigger(SUPERSTRONG.events.gmap.mapready);
				return _instance;

			};

			function _getMap (){

				return _instance._map;

			}

			function _getMarkers (){

				return _markers;

			}

			function _addMarker (markers,centerMap){

				if (!_instance._map || !markers){return;}
				var _map = _instance._map,
					_m = _markers;

				if ( markers.push !== undefined){
					var latLngs = [];
					for (var marker in markers){
						latLngs.push(new google.maps.LatLng(markers[marker].lat,markers[marker].lng));
						_m.push(_map.addMarker({
							lat: parseFloat(markers[marker].lat),
							   	lng:parseFloat(markers[marker].lng),
							title:markers[marker].title,
							infoWindow:markers[marker].infoWindow

						}));

					}

					 if (centerMap)_instance._map.fitLatLngBounds(latLngs);
				}

				if (typeof markers === 'object'){

				   _m.push(_map.addMarker({
					   lat: parseFloat(markers.lat),
					   lng:parseFloat(markers.lng),
					   title:markers.title,
					   infoWindow:markers.infoWindow

				   }));

				   if (centerMap){

					    _map.map.setCenter(new google.maps.LatLng(parseFloat(markers.lat),parseFloat(markers.lng)));

				   }
				}
			};

			function _clearMarkers (){

				for (var m in _markers){

					_markers[m].setMap(null);

				}

			}

			function _searchAddress (address,callback){

				if (!_instance._map || !address){return;}
				var map = _instance._map;
			  	_geocoder.geocode({
						'address': address,
						componentRestrictions: {
        					country: 'BE'
    					}
					}, function(results, status) {

				    if (status === google.maps.GeocoderStatus.OK) {
						callback.call(_instance,results);
			    	};
			  });
		  };

		  function _drawCircle(latLng,rad,color,zoomToFit){

			  var map = _instance._map;
			  var c = new google.maps.Circle({
		          center: latLng,
		          radius: rad,
		          map: _instance._map.map
			  });

			_polygons.push(c);

			if (zoomToFit){

				_instance._map.map.setZoom(_getZoomByBounds(c.getBounds()));
				_instance._map.map.setCenter(latLng)

			}

			return _instance;
		};

		function _clearCircles(){

			for (var p in _polygons){

				_polygons[p].setMap(null);

			}
		}

		function _getZoomByBounds( bounds ){

			var map = _instance._map.map
			var MAX_ZOOM = map.mapTypes.get( map.getMapTypeId() ).maxZoom || 21 ;
			var MIN_ZOOM = map.mapTypes.get( map.getMapTypeId() ).minZoom || 0 ;

			var ne= map.getProjection().fromLatLngToPoint( bounds.getNorthEast() );
			var sw= map.getProjection().fromLatLngToPoint( bounds.getSouthWest() );

			var worldCoordWidth = Math.abs(ne.x-sw.x);
			var worldCoordHeight = Math.abs(ne.y-sw.y);

			//Fit padding in pixels
			var FIT_PAD = 40;

			for( var zoom = MAX_ZOOM; zoom >= MIN_ZOOM; --zoom ){
			  if( worldCoordWidth*(1<<zoom)+2*FIT_PAD < $(map.getDiv()).width() &&
			      worldCoordHeight*(1<<zoom)+2*FIT_PAD < $(map.getDiv()).height() )
			      return zoom;
			}
			return 0;
		}

		return {

			getMap:_getMap,
			injectMap:_injectMap,
			getMarkers:_getMarkers,
			addMarker:_addMarker,
			clearMarkers:_clearMarkers,
			searchAdresse:_searchAddress,
			drawCircle:_drawCircle,
			clearCircles:_clearCircles,
			getZoomByBounds:_getZoomByBounds

			};
		}

		function __create (){
			return new Gmap();

		};

		if (!_instance){
			_instance = __create();
		};
		return _instance;

})()

GmapController.events = {};
GmapController.events.ready = "GmapController.events.mapReady";

