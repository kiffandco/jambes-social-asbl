console.log ("event filter")

var EventFilter = (function ($){

	
	var EventFilter = function (){

		var _instance = new SUPERSTRONG.EventDispatcher();
		SUPERSTRONG.EventDispatcher.call(_instance);
		this.data = [];
		this.filteredData = [];
		this.dirtyData = [];
		_instance.setData = this.setData.bind(this);
		_instance.getData = this.getData.bind(this);
		_instance.getFilterdData = this.getFilterdData.bind(this);
		_instance.applyFilters = this.applyFilters.bind(this);
		this._dispatcher = _instance;
		return _instance

	}

	_pEf = EventFilter.prototype;

	_pEf.setData = function (data){

		this.data = data;
		for (var d in this.data){
				
			this.data[d].include = true;
			this.data[d].exclude = false;

		}

	}

	_pEf.getData = function (){

		return this.data;

	}

	_pEf.getFilterdData = function (){

		return this.filteredData;

	}

	_pEf.applyFilters = function (filters){

		var self = this;
		self.filteredData = [];
		self.filteredData = applyFilter2($.extend (true,[],this.data.slice()));
		self._dispatcher.dispatchEvent(EventFilter.events.EVENT_FILTER_CHANGE);
		function applyFilter2 (data){
			
			var result = [],
				_data = data,
				_returnedData = {
					items:[],
					pristine:false

				}
				includeCount = 0
				;
			
			for (var d in _data){
				
				var conditions = {};
				var keys = Object.keys(_data[d].filters);
				_data[d].include = isSatisfy(_data[d],filters);
				
			}

			for (var d in _data){

				if (!_data[d].include) includeCount++

			}
			if (includeCount>=_data.length) _returnedData.pristine = true
			_returnedData.items = _data;
			return _returnedData;

		}

		function isSatisfy (data,filters){

			var satisfyCount = 0,
				cumulativeCount = 0
				;
			
			for (var filter in filters){
					
				var keys = Object.keys(filters[filter]);
				for (var key in keys){
					
					if (keys[key] === "cumulative" || keys[key] === "resultsCount" || keys[key] === "key" || keys[key] === "value" || keys[key] === "resultsLength"  )continue;
					evaluteFilter(data,keys[key])
					if (filters[filter].cumulative){
						
						
						
						if(data.filters.fields[keys[key]].value==filters[filter][keys[key]] || data.filters.fields[keys[key]].satisfied){
							
							satisfyCount++;	
							data.filters.fields[keys[key]].satisfied = true;

						}

					}else {

						if(data.filters.fields[keys[key]].value==filters[filter][keys[key]]) {
							
							satisfyCount++;	
							data.filters.fields[keys[key]].satisfied = true;

						};
						

					}
				 	
				}

				
			}
			
			
			return (satisfyCount)>=filters.length;

		}

		function evaluteFilter (data,filterKey){

			for(var filter in filters){

				if (filters[filter].hasOwnProperty(filterKey)){

					if(data.filters.fields[filterKey].value===filters[filter][filterKey]){

						data.filters.fields[filterKey].satisfied = true;

						

					}

				}
				

			}


		}

		

	}

	return EventFilter;

})(jQuery);
EventFilter.events = {};
EventFilter.events.EVENT_FILTER_CHANGE = "esperanzah.event.event_filter_change";