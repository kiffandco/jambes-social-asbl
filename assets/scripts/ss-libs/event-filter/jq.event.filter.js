(function ($){

	$.fn.eventFilter = function (opts){
		
		var data = [],
			defaults = {

				onChange:function (){}

			},
			options = $.extend({},defaults,opts),
			classModifier = options.classModifier || null,
			_instance = this
			_instance.filters = [];
			;

		_instance.eventFilter = new EventFilter();
		
		_instance.eventFilter.addListener(EventFilter.events.EVENT_FILTER_CHANGE,function(){

			options.onChange.call(_instance,_instance.eventFilter.getFilterdData(),_instance.filters)

		})

		$(classModifier).on('click',function (e){

			if (!$(e.currentTarget).is('a') )return;
			e.preventDefault()
			$(e.currentTarget).data('filter-enabled',
				$(e.currentTarget).data('filter-enabled') ? !$(e.currentTarget).data('filter-enabled') : "true"
				);
			applyFilter ();
		});

		$(classModifier).on('change',function (e){

			e.preventDefault()
			$(e.currentTarget).data('filter-enabled',
				$(e.currentTarget).data('filter-enabled') ? !$(e.currentTarget).data('filter-enabled') : "true"
				);
					
			_instance.update();

		});

		_instance.change = function (cb){

			options.onChange = cb;
			return _instance;

		}

		_instance.each(function (index,elm){

				var d = {};
				d.id = $(elm).attr('id');
				d.filters = {}
				var fields = $(elm).data()
				var keys =  Object.keys(fields);
				d.filters.fields = {}
				for (var key in keys){

					d.filters.fields[keys[key]] = {
						value:fields[keys[key]],
						satisfied :false

					}
					

				}
				
				
				data.push(d);


			})

		_instance.eventFilter.setData(data);

		_instance.update = function (){

			$(classModifier).each(function (index,elm){

				if($(elm).is(':checkbox')){

					$(elm).data('filter-enabled',$(elm).is(":checked").toString());
					

				}

			})
			
			applyFilter();
			return _instance;

		}

		function applyFilter (){

			var filters  	= [],
				filter		= null
				;

			_instance.filters = [];
			$(classModifier).each(function (index,elm){

				if ($(elm).data('filter-enabled') == "true" ){

					filter = {};
					filter[$(elm).data('filter-key')] = $(elm).data('filter-value');
					filter['value'] = $(elm).data('filter-value');
					filter['key'] = $(elm).data('filter-key');
					filter["cumulative"] = $(elm).data('cumulative');
					filters.push(filter);

				}

			})
			_instance.filters = filters;
			_instance.eventFilter.applyFilters(filters);

		}

		return _instance;

	};




})(jQuery)