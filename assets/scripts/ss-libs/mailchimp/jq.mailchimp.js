(function ($){

	$.fn.mailchimp = function (opts){

		var defaults = {

			submitSelector		:'.submit',
			responseSelector	:'#mce-responses',
			formWraperSelector	:'#mce-form',
			onError			:function (err){},
			onException		:function (data){},
			onSuccess		:function (data){},
			onSubmit		:function (data){},
			onInvalidate	:function (){},

		},
			options
			;
		options = $.extend({},defaults,opts);
		_instance = this,
		isValid = true;
		$(document.body).on('click',options.submitSelector,function (e){
            $elm = $(e.currentTarget);
            _instance.submit($elm.closest('form'));
            console.log ('subscribe');
            e.preventDefault();
         })

		_instance.onError = function (cb){

			options.onError = cb;
			return _instance;

		}

		_instance.onSuccess = function (cb){

			options.onSuccess = cb;
			return _instance;

		}

		_instance.onException = function (cb){

			options.onException = cb;
			return _instance;

		}

		_instance.onSubmit = function (cb){

			options.onSubmit = cb;
			return _instance;

		}

		_instance.onInvalidate = function (cb){

			options.onInvalidate = cb;
			return _instance;

		}

		_instance.invalidate = function (){

			isValid = false;
			return this;

		}

        _instance.submit = function ($form) {
           
           isValid = true;
           options.onSubmit.call(_instance,$form.find('input'));
           if (!isValid){

           		options.onInvalidate.call(_instance,$form.find('input'));
           		return;

           }     
			$.ajax({
				type 			:$form.attr('method'),
				url 			:$form.attr('action'),
				data 			:$form.serialize(),
				cache       	:false,
				dataType    	:'json',
				contentType 	:"application/json; charset=utf-8",

               	error :function(err) {

					options.onSuccess.call(_instance,err);

               	},
               	success :function(data) {
                    
                    if (data.result != "success") {

						options.onException.call(_instance,data);
                            
					} else {
                           
						options.onSuccess.call(_instance,data);
					}

				}
			});
		}  

		return _instance;      
	}

})(jQuery)