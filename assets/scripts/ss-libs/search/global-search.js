var SUPERSTRONG = (!SUPERSTRONG ? $({}) : SUPERSTRONG);
(function ($){

	SUPERSTRONG.GlobalSearch = {};
	SUPERSTRONG.GlobalSearch.init = function (){

		function keypressed(e){

			if (!e.charCode || $(document.body).hasClass('has-menu') || $(document.body).hasClass('has-overlay')) return ;
			var focused = $(':focus');
			if (focused && !focused.is(':input')){

				$(document).trigger('global-search.show',[e.charCode]);

			}

		}

		$(window).keypress(keypressed);



	};



})(jQuery);
