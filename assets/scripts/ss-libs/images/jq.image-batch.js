/**
 * Created by Rockerz on 22/11/16.
 */
$.fn.imageQueue = function (opts){

	var $dirtyElm       = null,
		instance        = this,
		loadingQueue    = [],

		defaults        = {

		silentLoad:false,
		onResume:function (){},
		onBeforeQueue:function (){},
		onStartQueue:function (){},
		onQueue:function (){},
		onBeforeImage:function (){},
		onStartImage:function (){},
		onImage:function (){}



	},
		options  = $.extend({},defaults,opts)

	;
	
	instance.completeCount      = 0;
	instance.totalCount         = instance.length;
	instance.resumed            = false;



	instance.each(function (index,elm){

		options.onBeforeImage.call(instance,$(elm));
		$dirtyElm = $(elm);
		$dirtyElm.data('src',$dirtyElm.attr('src'));
		$dirtyElm.attr('src','');


	});

	instance.loadQueue = function (){


		options.onBeforeQueue.call(instance,instance);
		instance.resumed = false;
		loadingQueue = [];
		instance.each (function (index,elm){

			options.onStartImage.call(instance,$(elm));
			var l =  loadOne($(elm).data('src'));
			l.done(function (){

				instance.completeCount++;
				options.onImage.call(instance,$(elm));

			});
			loadingQueue.push(l);

		});
		options.onStartQueue.call(instance,instance);
		$.when.apply($,loadingQueue).done(function (){

			options.onQueue.call(instance,instance);

		});

		return instance;
	};

	instance.resumeQueue = function (complete){

		loadingQueue = [];
		if (complete)options.onQueue.call(instance,instance);

	}

	instance.onBeforeImage = function (cb){

		options.onBeforeImage = cb;
		return instance;

	};

	instance.onStartImage = function (cb){

		options.onStartImage = cb;
		return instance;

	};

	instance.onImage = function (cb){

		options.onImage = cb;
		return instance;

	};

	instance.onImage = function (cb){

		options.onImage = cb;
		return instance;

	};

	instance.onBeforeQueue = function (cb){

		options.onBeforeQueue = cb;
		return instance;

	};

	instance.onStartQueue = function (cb){

		options.onStartQueue = cb;
		return instance;

	};

	instance.onQueue = function (cb){

		options.onQueue = cb;
		return instance;

	};

	instance.onResume = function (cb){

		instance.resumed = true;
		options.onResume = cb;
		return instance;

	};

	instance.setSilentLoading = function (silentLoad){

		options.silentLoad = silentLoad || true;

	}

	instance.isSilentLoad = function (){

		return options.silentLoad;


	}

	function loadOne (url){

		return $.ajax(
			{
				url:url,
				headers: {"Access-Control-Allow-origin": "*"}

			}

		);

	}

	return instance;

};