$.fn.dropdownFilter = function (opt){

        var defaults = {

            onSelect:function (){},
            useDefault      :true,
            defaultLabel    :"Tous",
            dataField       :'value',
            labelField      :'label',
            defaultLabel    :'All',
            defaultValue    :'All',
            keepOpen        :false

        },
        options = $.extend({},defaults,opt),
        instance = this;

        instance.select = function (cb){

            options.onSelect = cb;
            return instance;

        }

        instance.setSelected = function (value,dispatch){
      
            var elm = $(instance).find("[data-"+options.dataField+"='" + value + "']");
            if (dispatch)options.onSelect.call(instance,elm.data(options.dataField));
            instance.updateSelected(elm.data(options.dataField));
            return instance;

        }

        instance.updateSelected = function (value){

            $(instance).find("a.current-selected").text(value);

        }

        return instance.each(function (index,elm){

            if (options.useDefault){

                $(elm).find("ul").prepend(_itemFactory(options.defaultValue,options.defaultLabel));

            }
            instance.expandHeight = $(elm).find("ul").height();
            $(elm).data('expanded',false);
            $(elm).find("ul").height(0);
            $(elm).find("ul").css ({"display":"block"});
            $(document.body).on('click',_bodyClickHandler.bind(instance)); 
            $(elm).on("click",function (e){

                e.stopPropagation();
                options.onSelect.call(instance,$(this).data(options.dataField));
                _updateSelected($(this).data(options.labelField));

            })

            $(elm).find('a.current-selected').on('mouseup',_dropdownClickHandler.bind(instance))

            function _dropdownClickHandler (){

                if (!$(elm).data('expanded')){

                    $(elm).find("ul").height(instance.expandHeight);
                    $(elm).addClass('open');    

                }else {

                    $(elm).find("ul").height(0);
                    $(elm).removeClass('open');

                }
                
                $(elm).data('expanded',!$(elm).data('expanded'));                
                

            }

            function _bodyClickHandler (e){

                
                if (options.keepOpen) return;
                $(elm).find("ul").height(0);
                $(elm).data('expanded',false);
                $(elm).removeClass('open');

            }

            function _updateSelected (value){

                $(elm).find("a.current-selected").text(value);

            }

            function _setWidth(){

                var width = $(elm).width();
                $(elm).find("ul").each(function (index,elment){

                    if ($(elm).find(elment).width()>width) width = $(elm).find(elment).innerWidth()

                })
                $(elm).find('a.current-selected').width(width);

            }

            function _itemFactory (value,label){

                var elm = $('<li>').append(
                    $('<a>').data(options.labelField,label).data(options.dataField,value).text(label)

                )

                return elm;

            }

            _setWidth();

        })


        
    }