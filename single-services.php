<?php

$contentTitle           = get_the_title();
$contentFull            = get_the_content();
$termiID                = get_the_terms( get_the_ID(), 'servicestype');

$content                = apply_filters( 'the_content', $contentFull );
$content                = wpautop($content,false);
$content                = shortcode_unautop($content);

?>

<div class="page-wrapper">

  <div class="container">

    <header class="text-center padding-top-medium padding-bottom-medium">
      <h1><?php echo $contentTitle; ?></h1>
    </header>

    <div class="single-services-wrapper">

      <div class="return">
        <a class="button light-grey" href="javascript:history.back();">< Retour</a>
      </div>

    </div>

  </div>

</div>

<?php get_template_part('templates/flexible-content');?>
